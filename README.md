# Swiss-Knife-Bot
## The Swiss Army Knife is a versatile tool for every need. 
## Right now there are not many functions, but over time the functionality will expand. 
## Here is a short description of what the bot can do now.

### To display the current weather 
## For example `/weather Tokyo`
---
type: /weather [city]
---
Output:

🇯🇵 Country: JP

🌤️ Weather in Tokyo, 

🌡️ Temperature: 2.24°C

☁️ Description: Few Clouds

💨 Wind Speed: 10.29 m/s

### Find out the geo and provider ip address:
## For example: `/iplocate 8.8.8.8`
---
type: /iplocate [ip address]
---
Output:

🇺🇸 Country: United States

🗺 Region: VA

🌆 City: Ashburn

✉️ Zip Code: 20149

🕐 Timezone: America/New_York

📍 Latitude: 39.03

📍 Longitude: -77.5

🌐 Isp Provider: Google LLC

🏢 Organization: Google Public DNS

### To convert the exchange rate 
## For example: `/currency 1 eur usd`
---
type: /currency [number][currency][currency]
---
Output:
🗓 21-02-2023

💵 1.0 EUR is 1.0683 💳 USD

### Use `/warmon` to display rashist casualty statistics for the day

Output:
🗓 21-02-2023

🐷 Total combat losses of the russian pigs:

• Personnel Units: 144440

• Tanks: 3326

• Armoured Fighting Vehicles: 6562

• Artillery Systems: 2338

• MLRS: 471

• AA Warfare Systems: 243

• Planes: 299

• Helicopters: 287

• Vehicles Fuel Tanks: 5210

• Warships Cutters: 18

• Cruise Missiles: 873

• UAV Systems: 2023

• Special Military Equip: 226

• ATGM/SRBM Systems: 4

