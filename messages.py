msg_start = """
<b>Hello, I'm a Swiss Knife bot. 
\nTo display the current weather 
type: /weather [city] 
For example <code>/weather Tokyo</code>
\nTo convert the exchange rate 
type: /currency [number][currency][currency]
For example:  <code>/currency 1 eur usd</code>
\nFind out the geo and provider ip address:
type: /iplocate [ip address]
For example: <code>/iplocate 8.8.8.8</code>
\nUse /warmon to display rashist casualty statistics for the day</b>
"""
msg_help = ''

print(msg_start)
